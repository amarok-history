/* This file is part of the KDE project
    Copyright 2006-2008 Bart Cerneels <bart.cerneels@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/

#ifndef AMAROK_PODCASTSETTINGSDIALOG_H
#define AMAROK_PODCASTSETTINGSDIALOG_H

#include "PodcastMeta.h"

#include <KDialog>

namespace Ui {
    class PodcastSettingsBase;
}

class PodcastSettingsDialog : public KDialog
{
    Q_OBJECT

    public:
        explicit PodcastSettingsDialog( Meta::PodcastChannelPtr channel, QWidget* parent=0 );

        bool configure();

    protected:
        bool hasChanged();

    protected slots:
        void checkModified();
        void slotApply();

    private:
        void init();
        QString requesterSaveLocation();

        Ui::PodcastSettingsBase *m_ps;

        Meta::PodcastChannelPtr m_channel;
};

#endif /*AMAROK_PODCASTSETTINGSDIALOG_H*/
